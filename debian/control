Source: python-tackerclient
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-babel,
 python3-cliff,
 python3-ddt,
 python3-iso8601,
 python3-keystoneclient,
 python3-netaddr,
 python3-openstackdocstheme,
 python3-osc-lib,
 python3-oslo.i18n,
 python3-oslo.serialization,
 python3-oslo.utils,
 python3-reno,
 python3-requests,
 python3-requests-mock,
 python3-stestr,
 python3-stevedore,
 python3-subunit,
 python3-testtools,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/clients/python-tackerclient
Vcs-Git: https://salsa.debian.org/openstack-team/clients/python-tackerclient.git
Homepage: http://www.openstack.org/

Package: python-tackerclient-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: CLI and Client Library for OpenStack Tacker - doc
 Tacker is an official OpenStack project building a Generic VNF Manager (VNFM)
 and a NFV Orchestrator (NFVO) to deploy and operate Network Services and
 Virtual Network Functions (VNFs) on an NFV infrastructure platform like
 OpenStack. It is based on ETSI MANO Architectural Framework and provides a
 functional stack to Orchestrate Network Services end-to-end using VNFs.
 .
 This is the client API library for Tacker. This package contains the
 documentation.

Package: python3-tackerclient
Architecture: all
Depends:
 python3-babel,
 python3-cliff,
 python3-iso8601,
 python3-keystoneclient,
 python3-netaddr,
 python3-osc-lib,
 python3-oslo.i18n,
 python3-oslo.serialization,
 python3-oslo.utils,
 python3-pbr,
 python3-requests,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-tackerclient-doc,
Description: CLI and Client Library for OpenStack Tacker - Python 3.x
 Tacker is an official OpenStack project building a Generic VNF Manager (VNFM)
 and a NFV Orchestrator (NFVO) to deploy and operate Network Services and
 Virtual Network Functions (VNFs) on an NFV infrastructure platform like
 OpenStack. It is based on ETSI MANO Architectural Framework and provides a
 functional stack to Orchestrate Network Services end-to-end using VNFs.
 .
 This is the client API library for Tacker. This package contains the Python
 3.x module.
